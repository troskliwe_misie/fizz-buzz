import System.IO
import Data.List

--FizzBuzz using list iteration, pattern matching and nested function
fizzBuzz x =
    mapM_ fizzBuzzNested [1..x]
        where
            fizzBuzzNested b =
                putStr $
                case (b`mod`3,b`mod`5) of
                (0,0) -> "FizzBuzz "
                (0,_) -> "Fizz "
                (_,0) -> "Buzz "
                (_,_) -> show b ++ " "

fizzBuzz 100